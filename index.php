    <?php

        // Include Composer autoloader if not already done.
        include 'vendor/autoload.php';

        // Parse pdf file and build necessary objects.
        $parser = new \Smalot\PdfParser\Parser();
        $pdf    = $parser->parseFile('samples\MasoodSayed.pdf');
        //$pdf    = $parser->parseFile('samples\MahanteshKumbar.pdf');

        // Retrieve all pages from the pdf file.
        $pages  = $pdf->getText();

   //     echo $pages;
        echo ' ';

        $array = explode(' ', $pages);

        //print_r($array);

//First name  Last name

                $pattern = "/([A-Z][a-zA-Z]*)/";
             if (preg_match_all($pattern, $pages, $matches_out)) {
                // This prints an Array ("June 24", "August 13", "December 30")
               //  print_r($matches_out);
                echo "<h1>first name<h1>";
                echo "<h5>".$matches_out[0][1]."</h5>"; 
                echo "<h1>Last name<h1>";
                echo "<h5>".$matches_out[0][2]."</h5>"; 
                }

//Summary
function get_Summary($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }
 $summary = get_Summary($pages, 'Summary', 'Experience');
 echo "<h1>Summary</h1>";
 echo $summary;


//SKILLS
    $search_word="Skills";
    $end_word="Interviewing";
    echo "<h1>SKILLS</h1>";
    for($i=0;$i<=0;$i++)
    {
        for($j=0;$j<300;$j++)
        {


            if($matches_out[$i][$j]==$search_word)
            {
            
                do{
                    echo $matches_out[$i][$j+1].",";
                  
                    $j++;
                }while($matches_out[$i][$j]!=$end_word);
               break;
            }


        }
    }


//Interests

    function get_Interests($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }
    $interests = get_Interests($pages, 'Interests', 'Organizations');
    echo "<h1>Interests</h1>";
    echo $interests;
    echo "</br>";


//Email (multiple)
        $email_pattern="/[\w-]+@([\w-]+\.)+[\w-]+/";
        echo  "<h1>Emails</h1>";
        if (preg_match_all($email_pattern, $pages, $matches_out_email)) {
                 // This prints an Array ("June 24", "August 13", "December 30")
                
            for($i=0;$i<=0;$i++)
            {
                for($j=0;$j<sizeof($matches_out_email[0]);$j++)
                {


                    echo $matches_out_email[$i][$j]."<br>";


                }
        }



                }

    


//Profile Heading
function get_Profile_heading($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }

    if (preg_match_all($email_pattern, $pages, $matches_out_email1)) {
        //     // This prints an Array ("June 24", "August 13", "December 30")
            // print_r($matches_out_email1);
    }

    $pHeading =get_Profile_heading($pages, $matches_out[0][2], $matches_out_email1[0][0]);
    echo "</br>";
    echo "<h1>Profile Heading</h1>";
    echo $pHeading;
    echo "</br>";




//Education
    function get_education($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }
    $parsed_education = get_education($pages, 'Education','Interests');
    echo "</br>";
   
    $edu_pattern="/.*[^(\d\d\d\d - \d\d\d\d)]/";

    preg_match_all($edu_pattern, $parsed_education, $matches_out_edu);
        
    echo "</br>";
    $college_index=0;
    $csr_index=0;
    for($i=0;$i<=0;$i++)
    {
        for($j=0;$j<sizeof($matches_out_edu[0]);$j++)
        {
            if($j%2==0){
            $college[$college_index]=$matches_out_edu[$i][$j];
                $college_index++;
            }
            else{
                $csr_spcl_year[$csr_index]=$matches_out_edu[$i][$j];
                $csr_index++;
            }
        }
    }
   
$specialization_pattern="/\,(.*)\,/";
    for($i=0;$i<$csr_index||$i<$college_index;$i++)
    {
        if($i<$college_index)
        {
            echo "<h3>Education</h3>";
            //echo "</br>";
            echo $college[$i];
            echo "</br>";
        }
        if($i<$csr_index){
       // $csr_data=$csr_data.$csr_sp_yr[$i].",";
         if (preg_match_all($specialization_pattern, $csr_spcl_year[$i], $matches_out_speclztn)) {
        //     // This prints an Array ("June 24", "August 13", "December 30")
            // print_r($matches_out_edu);
             $course=strstr($csr_spcl_year[$i],',',true);
             echo $course;
               echo "</br>";
             echo $matches_out_speclztn[1][0];
               echo "</br>";
             $from_to = substr($csr_spcl_year[$i], strrpos($csr_spcl_year[$i], ',')+1);
             echo $from_to;
               echo "</br>";
            
    }
        }

    
    }

//recommendation
function get_Recmdtn($string, $start, $end){
        $string = " ".$string;
        $ini = strpos($string,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($string,$end,$ini) - $ini;
        return substr($string,$ini,$len);
    }
  echo "</br>";
    $recmdtn_pattern="people have recommended Masood";
        $rec_end_pattern="Page8";
    $recmdtn_data = get_Recmdtn($pages,$recmdtn_pattern,$rec_end_pattern);
 $array1 =explode('—',$recmdtn_data);
// print_r($array1);
$recdmtn_index=0;
$domain_index=0;

for($i=0;$i<=0;$i++)
{
    for($j=0;$j<sizeof($array1);$j++)
    {
        if($j==0)
        {
            $recmdtn[$recdmtn_index]=$array1[$j];
            $recdmtn_index++;
        }
        else{
            
            
           
            $recmdtn[$recdmtn_index] = strstr($array1[$j], '"');
            $recdmtn_index++;
            $domain[$domain_index] = strstr($array1[$j], '"', true); // As of PHP 5.3.0
            $domain_index++;
            
             
//echo $user;
        }
    }
    $domain[$domain_index]=$array1[$j-1];
   
}
    
    


echo "</br>";

        $name_index=0;
        $designation_index=0;
        $company_index=0;
        $rcmd_for_index=0;
for($i=0;$i<$domain_index+1;$i++)
{

    preg_match_all('/,/', $domain[$i], $m);
    //echo sizeof($m[0]);
    if(sizeof($m[0])==3)
    {
        $array2=explode(',',$domain[$i]);
        $name[$name_index]=$array2[0];
        $designtn[$designation_index]=$array2[1];
        $company[$company_index]=$array2[2];
        $array3=explode(' ',$array2[3]);
       // print_r($array3);
        $recmdtn_for[$rcmd_for_index]=$array3[1]." ".$array3[2];
        $name_index++;
        $designation_index++;
        $company_index++;
        $rcmd_for_index++;
    }
    if(sizeof($m[0])==1)
    {
        $name[$name_index]=strstr($domain[$i], ',', true);
       $rec_data=strstr($domain[$i], ',');
        $array4=explode(' ',$rec_data);
        $recmdtn_for[$rcmd_for_index]=$array4[1]." ".$array4[2];
        $name_index++;
        $rcmd_for_index++;
    }
  
}

for($i=0;$i<$name_index||$i<$recdmtn_index||$i<$designation_index||$i<$company_index||$i<$rcmd_for_index;$i++)
{
    if($i<$recdmtn_index)
    {
        echo "<h5>Recommendation</h5>"." ";
        echo  $recmdtn[$i];
        echo "</br>";
    } 
    if($i<$name_index)
    { 
         echo "<h5>name</h5>".":"." ";
         echo  $name[$i];
         echo "</br>";
    }
     if($i<$designation_index)
    {
          echo "<h5>designation</h5>".":"." ";
          echo  $designtn[$i];
          echo "</br>";
         
    }
    if($i<$company_index)
    {
      echo "<h5>company :</h5>".$company[$i];  
      //echo  $comp[$i];
      echo "</br>";   
    }
     if($i<$rcmd_for_index)
    {
         echo "<h5>recmdtn_for</h5>".":"." ";
         echo  $recmdtn_for[$i];
         echo "</br>";
    }
}
  // echo $csr_data;
  //$csr_array=explode(',',$csr_data);
  //print_r($csr_array);
     /*  0 2 4 6 8 10 -->college name 
         1 3 5 7 9 --->Course,specialization,from year to year.
         array of colleges and display
         1-9 expload and store array.
         0 3 6 --->course
         1 4 7 --->specialization
         2 5 8 --->year.

    //$fullstring = 'This is a long set of words that I am going to use.';

   



        // $filename = 'samples\MahanteshKumbar.pdf';
        // $rawdata = file_get_contents($filename);
        // if ($rawdata === false) {
        //     die('Unable to get the content of the file: '.$filename);
        // }
        // // configuration parameters for parser
        // $cfg = array('ignore_filter_errors' => true);
        // // parse PDF data
        // $pdf = new \Com\Tecnick\Pdf\Parser\Parser($cfg);
        // $data = $pdf->parse($rawdata);
        // // display data
        // var_dump($data);
    */

    ?>